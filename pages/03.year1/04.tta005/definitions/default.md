---
title: Thermodynamic definitions
---
* __System__ - An area in space containing matter that you want to observe. A system is a closed system if no matter moves between the system and the environment(across the boundary) or an open system if it does.
* __Fluid__ - A material that continuously deforms under sheer stress.
* __Boundary__ - A real or imaginary separation between a system and it's environment.
* __Heat(Q)__ - Energy exchanged between a system and environment(or other system) due to a difference in temperature
* __Work(W)__ - Energy transfer of energy due to the movement under a force between the system and it's environment. The work done is positive if the system does work on the environment and negative if the environment does work on the system.
* __Equilibrium__ - No state change between the system and it's environment
* __Specific quantities__ - Properties per unit mass