---
title: TTA005 Thermodynamics
---
##[Thermodynamic Definitions](/year1/tta005/definitions)

# General Properties of Gases
## Kinetic Theory
Kinetic theory provides a method of modelling the behaviour gasses in a simple way. It assumes the gas behaves as a 'perfect gas'. A perfect gas is assumed to have; rigid particles where collisions are fully elastic, volume of the particles is negligible (particles are a point in space) and the particles do not have any forces between each other.
The pressure of a container holding a gas can be defined as:
$$
p = \frac{2}{3}\frac{NkT}{V}\\
\text{Where}\\
p = \text{Absolute pressure}\\
T = \text{Absolute temperature}\\
V = \text{Container volume}\\
N = \text{Number of particles}\\
k = \text{Constant}
$$

This behaviour describes two important gas laws. Boyle's law states that $p\propto\frac{1}{V}$ and Charles' Law that $V\propto T$

!! These equations can be simplified to $pV = mRT$ where $mR = \frac{2}{3}kN$.
!! R is the gas constant e.g. for air $R = 287 J/kg K$

## Real Gases
Kinetic theory, Boyle's Law and Charles' Law all assume that a gas behaves perfectly. This is not the case in real life (Some diatomic gasses such as $O_2, H_2, N_2$ etc. behave very close to a perfect gas but only at high temperature and low pressure). Gases behave 'less perfectly' as pressure increases and temperature decreases since particles are very close and can no longer be considered as a single point in space and subsequently the forces between the particles are no longer negligible.

## Relative Atomic/Molecular Mass
Mass of atoms are expressed relative to the Unified Atomic Mass($u$) instead of in absolute terms.
$$
u = \frac{\text{Mass of C-12 atom}}{12} = 1.66\times10^{-27}kg
$$
This can then be used to equate the relative atomic mass of any molecule. For example Propane $C_{3}H_{8}$ has $M_{R} = 3\times12 + 1times8 = 44$

## Avogadro's Law
Avogadro's law states that the number of molecules in a gas is constant at any temperature or pressure.
$$
N = \frac{3}{2}k(pV/t)
$$

## Moles
Moles are used as a measure of the amount of a substance representing a fixed number of atoms, $N_{A}$.

!! Avogadro's constant: $6.022 \times 10^{26} = 1 kgmol \text{of carbon}$

$$
m = M \times n\\
\text{Where}\\
m = \text{mass of substance (kg)}\\
M = \text{molar mass (kg/kgmol)}\\
n = \text{number of moles(kgmol)}
$$

## Universal Gas Constant
!! The universal gas constant $R_{0} = 8315 J/kgmol K$

From kinetic theory ($pV_{0} = mRT$) we can write $pv_{0} = MRT$ or $pv_{0}/T = MR$ where $v_{0} = \text{molar volume} = \frac{V}{n}.

As a given gas has a constant mass, $M$ and gas constant, $R$, $pv_{0}/T$ must be constant. This means that we can calculate the gas constant using $MR = R_{0}$.