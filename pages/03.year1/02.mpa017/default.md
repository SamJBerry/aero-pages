---
title: MPA017 Engineering Materials
---

# Crystal Structure and Crystal Packing
## Crystal Structures

Crystals in a material are built up with crystal lattice. Crystal structures can be defined as a unit cell, which is the smallest group of atoms that has the same crystal structure.

### FCC - Face-centred cubic lattice

The FCC lattice is a cubic unit cell with one atom on each corner and one atom on each face.
Number of atoms in unit cell: $(8\times\frac{1}{8}) + (6\times\frac{1}{2}) = 4$

![](fcc-lattice.png?resize=200,200)

### BCC - Body-centred cubic lattice

The BCC lattice is a cubic unit cell with one atom in each corner of the lattice cube and one in the centre
Number of atoms in unit cell: $(8\times\frac{1}{8}) + 1 = 2$

![](bcc-lattice.png?resize=200,200)

### HCD - Hexagonal close-packed lattice

The HCD unit cell is a hexagonal shape with an atom on each corner, on the top and bottom faces and 3 atoms in a triangular arrangement completely inside of the unit cell.
Number of atoms in unit cell: $(12\times\frac{1}{6}) + (2\times\frac{1}{2}) + 3 = 6$

![](hcd-lattice.png?resize=200,200)

## Crystal Packing

The packing efficiency is defined as: $\frac{\text{Volume of atoms inside of unit cell}}{\text{Volume of unit cell}}$

# Ceramics

Ceramics are inorganic non-metallic solids.

The crystal structure can be broken down into cations and anions. The cation is the smaller positively charged ion and is surrounded by larger negatively charged ions. The crystal is considered stable if the cation is touching the anions. If the cation is 'floating' inside of the anions it is considered unstable.

The structure can be further characterised with; coordination number (the number of nearest neighbours) and stable condition (cation:anion size ratio).

Ceramic bonds can further be characterised by their Ionic Character. The amount of ionic character in a bond between A and B is linked to their electronegativities $X_A X_B$.

$$
\text{Ionic Character} = (1 - exp[-0.25(X_A-X_B)^2])
$$

# Polymers

* Polymers are large hydrocarbon molecules made of many monomers.
* Within polymer chains atoms are covalently bonded and Van der Waals forces exist between the chains in traditional cases.
* Polymers can exhibit a very wide range of characteristics.






# Example Questions

1. Calculate the packing efficiency for a FCC lattice.

$$
\text{Shortest path across touching atoms is 4r across face of unit cell.}\\
l^{2} + l^{2} = (4r)^{2}\\
2l^{2} = 16r^{2}\\
l = \sqrt{8r^{2}} = 2\sqrt{2}r\\
\text{Volume of cell} = l^{3} = 16\sqrt{2}r^{3}\\
\text{Volume of atoms in cell} = 4 \times \frac{4}{3} \pi r^{3}\\

\text{Packing ratio} = \frac{\text{Volume of atoms in cell}}{\text{Volume of cell}}\\
=\frac{\frac{16}{3} \pi r^{3}}{16\sqrt{2}r^{3}}\approx0.74\\
$$